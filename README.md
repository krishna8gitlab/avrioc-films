# Films API

# Introduction

This repo has the implementation of the simple api for Films and Review Comments.

# Feature Description:
### Implement RESTful API to manage films
Films should have fields:

        Name
        Description
        Realease Date
        Rating
        Ticket Price
        Country
        Genre
        Photo

1. Create frontend page /films/ to show all films through API. 1 film per 1 page.
2. add redirect from / to /films/
3. implement frontend page /films/film-slug-name to show specific film. URL should have film's slug.
4. implement frontend page /films/create with form to create new film.
5. Registration and Authentication
6. add possibility to post comments for each films. Fields "Name" and "Comment" are required.
7. Only registered users can post comments

# Prerequisites
In order to start and run this application, you will need:

* [Node.js](https://nodejs.org/en/download/) (newer the better - Using the current Long Term Stable version would be recommended)
* npm (installed with Node.js)
* [mongodb](https://www.mongodb.com/try/download/community): Mongo DB Nees to be installed.

This application is tested with the following setup:
* machine: `Ubuntu 18.04`
* node: `v12.22.0`
* npm: `6.14.11`
* MongoDB: ` Community Server v4.0.23`

# Setup

To get started:

```
$ npm install
```

# Configuration 

The application uses a configuration file `.env` to specify the port that it listens 
on plus some logging parameters and how it connects to a mongodb database.

The supplied `.env` file is already set up to use MongoDB on localhost port 27017 without any username/password.  Change these values if your Mongo instance is on another host or port, or requires a password to connect.

```
NODE_ENV=development
APP_PORT=8080
MONGO_URI=mongodb://localhost:27017/avrioc-films?authSource=admin
MONGO_URI_TESTS=mongodb://mongodb:27017/express-rest-es2017-boilerplate
LOG_LEVEL=debug
APP_SECRET=3ad84a79-c897-433d-99f4-d4e1231d578d
```

# Load Sample Data

To load sample datasets, run:

```
npm run load_data
```
### NOTE: 
`npm run load_data` needs to be run first otherwise the application fails with small errors. This command loads 3 collections into mongo database. The database is named as: `avrioc-films`
1. `users`: Following users are configured. There is no explict user registration in this application.
We have to use these configured users.
    * `avrioc/pass1234`
    * `krishna/pass1234`
2. `films`: 3 Films are loaded.
2. `comments`: 3 Comments, one for each film are loaded.

# Development Workflow

In order to speed up development, we can run the application using `nodemon`, so that any 
changes to source code files cause the server to reload and start using our changes.

```
npm run dev
```

Edit code, application will hot reload on save.

If we want to run without `nodemon`, we can just use:

```
npm start
```

But we will then need to stop the server and restart it when you change code.

# Accessing the Front End Web Application

We should be able to see the front end at: 

```
http://localhost:8080/
```
This will redirect us to 
```
http://localhost:8080/films
```
1. There are two navigation buttons `<< Previous` and `Next >>` to get the previous and next films respectively.
2. A `login` button for the user to login and post the comments on a particular movie.
3. A `logout` button to logout from the current session.
4. An Error Page is displayed with appropriate message in two cases:
    - If the user is NOT logged in and try to post a comment.
    - If Invalid username/password are entered.
 
# Running Tests

The project is setup to use [Jest](https://jestjs.io/en/) for testing.  To run the tests:
```
npm run test
```

# Linting
This project uses [ESLint](https://eslint.org/).

* The file `.eslintrc` contains a short list of rules that have been disabled for this project.
* The file `.eslintignore` contains details of paths that the linter will not consider when 
linting the project.

To run the linter:

```
npm run lint
```

# Frameworks/Middlewares Used
1. `Express`: For Rest API 
2. `Mongoose`: For Data Store
3. `Passport`: For Authentication
4. `Jest`: For Unit Tests
5. `EJS`: View Template Engine
6. `Multer`: For uploading images


