const multer = require('multer');
const logger = require('../config/logger')
const path = require('path');

// Set Storage for Films Images
let storage = multer.diskStorage({
    destination: (req, file, cb) => {
        const storagePath = `${path.join(__dirname, '../public/film_images')}`;
        cb(null, storagePath)
    },
    filename: (req, file, cb) => {
        cb(null, `${file.fieldname}-${Date.now()}.png`)
    }
});

const upload = multer({ storage: storage });

module.exports = upload;