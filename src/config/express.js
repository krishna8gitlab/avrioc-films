const morgan = require('morgan');
const cors = require('cors');
const path = require('path');
const helmet = require('helmet');
const passport = require('passport');
const express = require('express');
const session = require('express-session');
const routes = require('../routes');
const logger = require('./logger');
const { secret } = require('./vars');
const Users = require('../models/user.model');

/**
* Express instance
* @public
*/
const app = express();

app.use(morgan('tiny', { stream: logger.stream }));

// parse body params and attach them to req.body
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// secure apps by setting various HTTP headers
app.use(helmet());

// enable CORS - Cross Origin Resource Sharing
app.use(cors());

// Use public folder for images
app.use('/public/', express.static('../public'));

// Error Handling
app.use((err, req, res, next) => {
    res.locals.error = err;
    const status = err.status || 500;
    res.status(status);
});

// Authenticated User Session Handling
app.use(session({
    secret,
    resave: false,
    saveUninitialized: true,
    cookie: { maxAge: 60 * 60 * 1000 } // 1 hour
}));
app.use(passport.initialize());
app.use(passport.session());

// Set EJS as templating engine
var appDir = path.dirname(require.main.filename);
app.set("view engine", "ejs");
app.set('views', path.join(appDir, '/views/'));

// mount api routes
app.use('/', routes);

module.exports = app;
