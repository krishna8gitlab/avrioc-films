const winston = require('winston');
const chalk = require("chalk");

this.logFormat = winston.format.printf(info => {
  const formattedDate = info.timestamp.replace('T', ' ').replace('Z', '');
  return `${formattedDate} | ${info.level} |${info.message}`;
});

const logger = winston.createLogger({
  level: global.loglevel || 'info',
  format: winston.format.combine(
    winston.format.colorize(),
    winston.format.timestamp(),
    winston.format.align(),
    this.logFormat
  ),
  transports: [new winston.transports.Console({})]
});

logger.stream = {
  // Write the text in 'message' to the log.
  write: (message) => {
    // Removes double newline issue with piping morgan server request
    // log through winston logger.
    logger.info(message.length > 0 ? message.substring(0, message.length - 1) : message);
  },
};

module.exports = logger;
