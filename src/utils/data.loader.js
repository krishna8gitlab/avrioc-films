const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');
const logger = require('../config/logger');
const User = require('../models/user.model');
const Film = require('../models/film.model')
const Comment = require('../models/comment.model')
const { mongo } = require('../config/vars');

/**
 * Load Users into DB from users.json
 */
const loadUsers = async () => {
    let users = require('../resources/data/users.json');
    for await (const user of users) {
        let _user = new User(user);
        await _user.save();
        logger.info(`User with the username ${user.username} saved successfully!!`);
    }
    const count = await User.countDocuments();
    logger.info(`${count} User(s) are now in database!!`)
    logger.info("---------------------------------------------------------");
}

/** 
 * Load Films into db from films.json
 */
loadFilm = async (film) => {
    film.photo = {
        data: fs.readFileSync(path.join(`${__dirname}/../resources/film_images/${film.photo}`)),
        contentType: 'image/png'
    }
    film = new Film(film);
    await film.save();
    logger.info(`Film ${film.name} loaded!!`);
}

/**
 * Load Comments into db from comments.json
 */
loadFilms = async () => {
    let films = require('../resources/data/films.json');
    for await (const film of films) {
        await loadFilm(film);
    }
    const count = await Film.countDocuments();
    logger.info(`${count} film(s) are now in database!!`)
    logger.info("---------------------------------------------------------");
}

loadComments = async () => {
    let comments = require('../resources/data/comments.json');
    for await (const comment of comments) {
        let _comment = new Comment(comment);
        await _comment.save();
    }
    const count = await Comment.countDocuments();
    logger.info(`${count} comment(s) are now in database!!`)
    logger.info("---------------------------------------------------------");
}

closeMongoConnection = async () => {
    mongoose.connection.close(() => logger.info("Mongoose connection closed!!"));
}

mongoose
    .connect(mongo.uri, {
        useCreateIndex: true,
        keepAlive: 1,
        useNewUrlParser: true,
        useUnifiedTopology: true
    })
    .then(() => logger.info('MongoDB connection established!!'))
    .then(() => loadUsers())
    .then(() => loadFilms())
    .then(() => loadComments())
    .then(() => closeMongoConnection())
    .catch((err) => {
        logger.error(err)
        closeMongoConnection();
    });
