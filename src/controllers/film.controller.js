const Film = require("../models/film.model");
const Comment = require('../models/comment.model');
const logger = require("../config/logger");
const httpStatus = require('http-status')
const fs = require('fs');
const path = require('path');
const { request } = require("../config/express");

/**
 * Render films
 */
exports.renderFilms = async (req, res, next) => {
    try {
        const { name } = req.query;
        if (name) {
            this.getByName(req, res, next);
        } else {
            const films = await Film.list(req.query);
            logger.info('Film listed successfully');
            const comments = await Comment.getByFilmId(films.films[0]._id);
            films.films[0].comments = comments;
            if (req.user) {
                films.user = req.user;
            } else {
                films.user = undefined;
            }
            res.render('films/index', films);
        }
    } catch (error) {
        logger.error(error);
        next(error);
    }
};

/**
 * Create new film
 * @public
 */
exports.create = async (req, res, next) => {
    try {
        const filmObj = req.body;
        filmObj.genre = filmObj.genre.split(',').map(g => g.trim());
        filmObj.photo = {
            data: fs.readFileSync(path.join(`${__dirname}/../public/film_images/${req.file.filename}`)),
            contentType: 'image/png'
        }
        const film = new Film(filmObj);
        await film.save();
        res.status(httpStatus.CREATED);
        res.redirect('/films');
    } catch (error) {
        logger.error(error);
        next(error);
    }
};

/**
 * Get Film by film name
 * @public
 */
exports.getByName = async (req, res, next) => {
    try {
        const film = await Film.findOne(req.query);
        if (!film) {
            res.status(httpStatus.NOT_FOUND);
            res.render('films/nofilm', req.query);
        }
        const comments = await Comment.getByFilmId(film._id);
        film.comments = comments;
        res.status(httpStatus.OK);
        res.render('films/byname', { user: req.user, film, page: undefined });
    } catch (error) {
        logger.error(error);
        next(error);
    }
};

/**
 * Post a Review Comment on a film
 * @public
 */
exports.postComment = async (req, res, next) => {
    try {
        const { filmId } = req.params;
        const { page } = req.query;
        const { text } = req.body;
        const { user } = req;
        const comment = new Comment({
            filmId,
            text,
            createdBy: user.username,
        });
        await comment.save();
        res.status(httpStatus.CREATED);
        res.redirect(`/films?page=${page}`);
    }
    catch (error) {
        logger.error(error);
        next(error);
    }
}
