const passport = require('passport');
const logger = require('../config/logger');
const httpStatus = require('http-status');

exports.login = (req, res, next) => {
    passport.authenticate('local',
        (err, user, info) => {
            if (err) {
                logger.error(err);
                return next(err);
            }

            if (!user) {
                res.status(httpStatus.UNAUTHORIZED);
                res.render('auth/fail', { info: info });
            }

            req.logIn(user, function (err) {
                if (err) {
                    logger.error(err);
                    return next(err);
                }
                logger.info(`User(${user.username}) logged in successfully!`)
                req.user = user;
                res.redirect('/films');
            });

        })(req, res, next);
}
