require('module-alias/register');
const logger = require('./config/logger');
const app = require('./config/express');
const mongoose = require('./config/mongoose');
const { port, env } = require('./config/vars');

// open mongoose connection
mongoose.connect().then(

);

// listen to requests
app.listen(port, () => {
    logger.info('---------------------------------------------------------------------');
    logger.info(`Server runing on port ${port} ...`);
    logger.info(`Access URLs:`);
    logger.info(`       Local:   http://localhost:8080/`);
    logger.info(`       Profiles: ${env}`);
    logger.info('---------------------------------------------------------------------');
});

module.exports = app;

