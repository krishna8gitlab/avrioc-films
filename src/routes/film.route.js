const express = require('express');
const controller = require('../controllers/film.controller');
const upload = require('../config/multer')
const connectEnsureLogin = require('connect-ensure-login');

const router = express.Router();

router
    .route('/')
    .get(controller.renderFilms)
    .post(upload.single('photo'), controller.create);

router
    .route('/create')
    .get((req, res) => res.render('films/create'))

router
    .route('/:filmId/comment')
    .post(connectEnsureLogin.ensureLoggedIn('/login/error'), controller.postComment);

module.exports = router;
