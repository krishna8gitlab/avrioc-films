const express = require('express');
const router = express.Router();
const userController = require('../controllers/user.controller');

router
    .route('/')
    .get((req, res) => res.render('auth/login.ejs'))
    .post(userController.login);

router
    .route('/error')
    .get((req, res) => res.render('auth/not_loggedin_error.ejs'));

router
    .route('fail')
    .get((req, res) => res.render('auth/fail.ejs'));

module.exports = router;
