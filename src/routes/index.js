const express = require('express');
const router = express.Router();
const logger = require('../config/logger');

const filmRoutes = require('./film.route');
const loginRoutes = require('./login.route');

router
    .route('/')
    .get((req, res) => res.redirect('/films'));


router
    .route('/logout')
    .get((req, res) => {
        logger.info(`User with username '${req.user.username}' logged out successfully!!`);
        req.logout();
        res.redirect('/films');
    });

router.use('/films', filmRoutes);
router.use('/login', loginRoutes);

module.exports = router;
