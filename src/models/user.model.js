const mongoose = require('mongoose');
const passportLocalMongoose = require('passport-local-mongoose');
const passport = require('passport');
const bcrypt = require('bcryptjs');
const { env } = require('../config/vars');
const logger = require('../config/logger');

/**
* User Roles
*/
const roles = ['ADMIN', 'USER', 'GUEST'];

/**
 * User Schema
 * @private
 */
const userSchema = new mongoose.Schema({
    email: {
        type: String,
        match: /^\S+@\S+\.\S+$/,
        required: true,
        unique: true,
        trim: true,
        lowercase: true,
    },
    password: {
        type: String,
        required: true,
        minlength: 6,
        maxlength: 128,
    },
    firstName: {
        type: String,
        maxlength: 128,
        index: true,
        trim: true,
    },
    lastName: {
        type: String,
        maxlength: 128,
        index: true,
        trim: true,
    },
    role: {
        type: String,
        enum: roles,
        default: 'USER',
    }
}, {
    timestamps: true,
});

/**
 * Add your
 * - pre-save hooks
 * - validations
 * - virtuals
 */
userSchema.pre('save', async function save(next) {
    try {
        if (!this.isModified('password')) return next();

        const rounds = env === 'test' ? 1 : 10;
        const hash = await bcrypt.hash(this.password, rounds);
        this.password = hash;

        return next();
    } catch (error) {
        return next(error);
    }
});


userSchema.plugin(passportLocalMongoose);

const Users = mongoose.model('User', userSchema, 'users');

// Passport Local Strategy
passport.use(Users.createStrategy());

// To use with sessions
passport.serializeUser(Users.serializeUser());
passport.deserializeUser(Users.deserializeUser());

module.exports = Users;
