const mongoose = require("mongoose")
const Schema = mongoose.Schema
const httpStatus = require('http-status');

var filmSchema = new Schema({
    name: {
        type: String,
        required: true,
        index: true
    },
    description: {
        type: String,
        required: true,
    },
    releaseDate: {
        type: Date,
        required: true,
    },
    rating: {
        type: Number,
        required: true,
    },
    ticketPrice: {
        type: Number,
        required: true,
    },
    country: {
        type: String,
        required: true,
    },
    genre: [{
        type: String,
        required: true,
    }],
    photo: {
        data: Buffer,
        contentType: String
    }
}, { timestamps: true });

filmSchema.method({
    transform() {
        const transformed = {};
        const fields = [
            '_id',
            'name',
            'description',
            'releaseDate',
            'rating',
            'ticketPrice',
            'country',
            'genre',
            'photo',
            'createdAt'
        ];

        fields.forEach((field) => {
            transformed[field] = this[field];
        });

        return transformed;
    },
})

filmSchema.statics = {

    async getByName(name) {
        try {
            let film = await this.find({ name }).exec();
            if (film) {
                return film;
            }

            throw new Error({
                message: `Film with the name '${name}' does not exist`,
                status: httpStatus.NOT_FOUND,
            });
        } catch (error) {
            throw error;
        }
    },

    async list({ page = 1, perPage = 1 }) {
        try {
            page = parseInt(page)
            perPage = parseInt(perPage)
            let count = await this.countDocuments();
            let films = await this.find({})
                .skip(perPage * (page - 1))
                .limit(perPage)
                .exec();
            films = films.map(film => film.transform());
            const pages = Math.ceil(count / perPage);
            return { films, count, page, prev: page - 1, next: page + 1 }
        } catch (error) {
            throw error;
        }
    },
};

module.exports = mongoose.model('Film', filmSchema);
