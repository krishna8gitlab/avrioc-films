const mongoose = require("mongoose")
const Schema = mongoose.Schema
const httpStatus = require('http-status');

var commentSchema = new Schema({
    filmId: {
        type: Schema.Types.ObjectId,
        ref: 'Film',
        index: true
    },
    text: {
        type: String,
        required: true,
    },
    createdBy: {
        type: String,
        required: true,
    },
}, { timestamps: true });

commentSchema.method({
    transform() {
        const transformed = {};
        const fields = [
            '_id',
            'filmId',
            'text',
            'createdBy',
            'createdAt'
        ];

        fields.forEach((field) => {
            transformed[field] = this[field];
        });

        return transformed;
    },
});

commentSchema.statics = {

    async getByFilmId(filmId) {
        try {
            let comments = await this.find({ filmId })
                .sort({ createdAt: -1 })
                .limit(5)
                .exec();
            if (comments) {
                comments = comments.map(comment => comment.transform())
                return comments;
            }
            return [];
        } catch (error) {
            throw error;
        }
    },

    async list({ page = 1, perPage = 1 }) {
        try {
            page = parseInt(page)
            perPage = parseInt(perPage)
            let count = await this.count();
            let films = await this.find({})
                .skip(perPage * (page - 1))
                .limit(perPage)
                .exec();
            films = films.map(film => film.transform());
            const pages = Math.ceil(count / perPage);
            return { films, count, prev: page - 1, next: page + 1 }
        } catch (error) {
            throw error;
        }
    },
}

module.exports = mongoose.model('Comment', commentSchema);
