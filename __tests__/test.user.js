const User = require('../src/models/user.model');
const mongoose = require('mongoose');

describe("------------- Users ---------------- ", () => {

    beforeAll(async () => {
        const url = `mongodb://localhost:27017/avrioc-films-test?authSource=admin`
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        await User.deleteMany({});
    });

    afterAll(async () => await mongoose.connection.close())

    test("Save 2 Users into a Database", async () => {
        const users = require('../src/resources/data/users.json');
        for await (const user of users) {
            let _user = new User(user);
            await _user.save();
        }
        const count = await User.countDocuments();
        expect(count).toBe(2);
    });

    test("User Save with required fields missing", async () => {
        let oneMoreUser = {
            "role": "USER",
            "username": "avrioc",
        };
        oneMoreUser = new User(oneMoreUser);
        oneMoreUser.save().catch(err => {
            expect(err.name).toBe('ValidationError');
            expect(err.message).toBe('User validation failed: password: Path `password` is required., email: Path `email` is required.');
        });
    });
});

