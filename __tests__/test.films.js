const Film = require('../src/models/film.model');
const mongoose = require('mongoose');
const fs = require('fs');
const path = require('path');

describe("-------------- Films ------------", () => {

    beforeAll(async () => {
        const url = `mongodb://localhost:27017/avrioc-films-test?authSource=admin`
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        await Film.deleteMany({});
    });

    afterAll(async () => await mongoose.connection.close())

    test("Save 3 Films into Database", async () => {
        const films = require('../src/resources/data/films.json');
        for await (const film of films) {
            film.photo = {
                data: fs.readFileSync(`./src/resources/film_images/${film.photo}`),
                contentType: 'image/png'
            }
            let _film = new Film(film);
            await _film.save();
        }
        const count = await Film.countDocuments();
        expect(count).toBe(3);
    });

    test("Save Fails with Unique Key violation", async () => {
        let oneMoreFilm = {
            "_id": "61162b051faf9d02ed65861d",
            "genre": [
                "Action",
                "Comedy",
                "Animation"
            ],
            "name": "Kungfu Panda",
            "description": "Kungu Panda",
            "releaseDate": "2021-08-05",
            "rating": 4,
            "ticketPrice": 150,
            "country": "India",
            "photo": "kungfupanda.jpg"
        };
        oneMoreFilm.photo = {
            data: fs.readFileSync(`./src/resources/film_images/${oneMoreFilm.photo}`),
            contentType: 'image/png'
        }

        oneMoreFilm = new Film(oneMoreFilm);
        oneMoreFilm.save().catch(err => {
            expect(err.name).toBe('MongoError');
            expect(err.message).toBe('server is closed');
        });
    });
});
