const Comment = require('../src/models/comment.model');
const mongoose = require('mongoose');

describe("------- Comments -----------", () => {

    beforeAll(async () => {
        const url = `mongodb://localhost:27017/avrioc-films-test?authSource=admin`
        await mongoose.connect(url, {
            useNewUrlParser: true,
            useUnifiedTopology: true
        })
        await Comment.deleteMany({});
    });

    afterAll(async () => await mongoose.connection.close())

    test("Save 3 Comments into a Database", async () => {
        const comments = require('../src/resources/data/comments.json');
        for await (const comment of comments) {
            let _comment = new Comment(comment);
            await _comment.save();
        }
        const count = await Comment.countDocuments();
        expect(count).toBe(3);
    });

});


